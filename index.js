// Array Traversal

/*
	Syntax:
		let/const arrayName = [elementA, elementB, elementC...];
*/

// Storing numeric elements
let grades = [98.5, 94.3, 89.2, 90.1];
// Storing string elements
let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];

let mixedArr = [12, 'Asus', null, undefined, {}]; // This is not recommended

let tasks = [
	'drink html',
	'eat javascript',
	'inhale css',
	'bake sass',
];

// Creating an array with values from variables
let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "New Delhi";

let cities = [city1, city2, city3];

console.log(tasks);
console.log(cities);

// .length property - allows us to get and set the total number of elements in an array

console.log("Using .length property");
console.log(tasks.length);
console.log(cities.length);

let fullName = "Jamie Noble";
console.log(fullName.length);

tasks.length = tasks.length - 1;
console.log("Remaining elements: " +  tasks.length);
console.log(tasks);

cities.length--;
console.log(cities)

let theBeatles = ["John", "Paul", "Ringo", "George"];
theBeatles.length++;
console.log(theBeatles);

// Accessing elements of an Array
// Syntax: arrayName[arrayIndex]

console.log("Accessing elements through indexes")
console.log(grades[0]);
console.log(computerBrands[3]);
// Accessing an array element that does not exist
console.log(grades[20]);

let lakersLegends = ['Kobe', 'Shaq', 'LeBron', 'Magic', 'Kareem'];
/*
	Mini Activity
		Access elements 'Kobe', 'Lebron', 'Kareem' in lakesLegends array
*/
console.log(lakersLegends[0]);
console.log(lakersLegends[2]);
console.log(lakersLegends[4]);

// Save/store array items/elements in another variable
console.log("Array element stored in another variable");
let currentLakers = lakersLegends[2];
console.log(currentLakers);

// Reassign array elements using the elements' indices
console.log("Arary reassignment");
console.log("Array before reassignment");
console.log(lakersLegends);
lakersLegends[2] = "Pau Gasol";
console.log("Array after reassignment");
console.log(lakersLegends);

// Accessing the last element of an array
console.log("Accessing the last element of an array")
let bullsLegends = ['Jordan', 'Pippen', 'Rodman', 'Rose', 'Kukoc'];

let lastElementIndex = bullsLegends.length - 1;
console.log(bullsLegends[lastElementIndex]);

// Directly access the last element
console.log(bullsLegends[bullsLegends.length - 1]);

// Adding items into the array
console.log("Adding items into the array");
let newArr = [];
console.log(newArr[0]);

newArr[0] = 'Cloud Strife';
console.log(newArr);
console.log(newArr[1]);
newArr[1] = 'Tifa Lockhart';
console.log(newArr);

// Looping over an array
console.log('Looping over an array');
for(let index = 0; index < newArr.length; index++){
	console.log(newArr[index])
}

let numArr = [5, 12, 30, 46, 40];

for(let index = 0; index < numArr.length; index++){
	if(numArr[index] % 5 === 0){
		console.log(numArr[index] + " is divisible by 5");
	}
	else{
		console.log(numArr[index] + " is not divisible by 5");
	}
}

// Multidimensional array

console.log("Multidimensional array");
/*
	Syntax:
		2 x 3 Two dimensional array
		                  0                  1
		twoDim = [[arr1, arr2, arr3],[arr4, arr5, arr6]]
		             0    1      2      0     1     2
*/

let twoDim = [['arr1', 'arr2', 'arr3'],['arr4', 'arr5', 'arr6']];
console.log(twoDim[0][0]);
// Mini activity = access elements arr3, arr4, arr6
console.log(twoDim[0][2]);
console.log(twoDim[1][0]);
console.log(twoDim[1][2]);
